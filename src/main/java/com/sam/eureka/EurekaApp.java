package com.sam.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.context.annotation.Configuration;

/**
 * @SpringBootApplication注解指定这是一个SpringBoot的应用程序，不加就会报异常 Unable to start
 *                                                       ServletWebServerApplicationContext
 *                                                       due to missing
 *                                                       ServletWebServerFactory
 */
@EnableEurekaServer
@SpringBootApplication
@Configuration
public class EurekaApp {
	public static void main(String[] args) {
		SpringApplication.run(EurekaApp.class, args);
	}
}
